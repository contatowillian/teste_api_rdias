<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produtos;
use App\Especialidades;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $retorno_view['qtd_produtos_ativos'] =  DB::table('produtos')->whereNull('deleted_at')->get();
        $retorno_view['qtd_produtos_inativo'] = DB::table('produtos')->wherenotNull('deleted_at')->get();; 

        return view('home',compact('retorno_view'));
    }
}
