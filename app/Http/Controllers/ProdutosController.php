<?php
namespace App;
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Produtos;
use App\FilaProduto;
use App\CategoriaProdutos;
use App\Jobs\InseriProdutosTabela;
use DB;
	
/**
* Criado Por Willian Batista 19/03/2020
* Controller que trata a parte de produtos do sistema
* Teste para a R-Dias
*/


/************** Class da API e do Crud dos produtos ****************/
class ProdutosController extends Controller
{

	 /**
	  * Exibir um registo específico.
	  *
	  * @param  int  $id
	  * @return Response
	  */

	/* Função que faz a  busca de um produto especifico pelo ID*/ 
	public function show($id) {
	     return Produtos::find($id);
	}

	/* Função que busca os dados do produto*/ 
	public function upload_planilha() {
	  
	  /*Verifica se o usuario esta autenticado, se nao estiver manda de volta pra pagina de login*/
	  return view('produtos.upload_planilha',compact('retorno_view'));
	}

	public function recebe_arquivo_produtos(Request $request) {
	  
	  // pega os dados do Post
	  $dados = $request->all();
	  $linha_arquivo= array();

	  $linha_atual_arquivo = $dados['linha_atual_arquivo'];
	  $quebra_colunas = explode(',',$dados['linha_arquivo']) ;
	  $categoria_id= '';

	  $coluna= 1;

	  $retorno = array();
	  $retorno['resultado'] ='' ;
	  $retorno['Mensagem']='';
	  $verifica_produto_existe = false;

	  /* Ignora a primeira coluna */
	  if($linha_atual_arquivo!=1){

	  		/* Quebra as linhas*/
	  		foreach($quebra_colunas as $array_linha){
	  			/* Verifica se a coluna categoria ja existe, caso nao exista faz o cadastro*/
	  		    if($coluna==5 and $array_linha!=''){
	  		    	  $categoria_id = $this->verifica_categoria_existe(utf8_decode($array_linha));
			 	}

			 	/* Verifica se a coluna categoria ja existe, caso nao exista faz o cadastro*/
	  		    if($coluna==1 and $array_linha!=''){
	  		    	  $verifica_produto_existe = $this->verifica_produto_existe($array_linha);
			 	}
			 
	  		
	  			/* Faz as verificações de erro no arquivo */
	  			if(strlen($array_linha)>255){
	  				$retorno['resultado'] ='erro' ;
			 		$retorno['Mensagem'] ='Erro na linha '.$linha_atual_arquivo.', Coluna deve ter no máximo 255 caracteres';
	  			}else if($coluna<=5 and  $array_linha==''){
  					$retorno['resultado'] ='erro' ;
			 		$retorno['Mensagem'] ='Erro na linha '.$linha_atual_arquivo.', todas as colunas são obrigatorias';
  				}else if($coluna>5 and $array_linha!=''){
			  		$retorno['resultado'] ='erro' ;
			 		$retorno['Mensagem'] ='Erro na linha '.$linha_atual_arquivo.', quantidade colunas maior que o permitido';
			 	}
			  	$coluna++;
	   		}


	   		if($retorno['resultado'] !='erro'){


  				 if($this->grava_produto_fila($dados['linha_arquivo'],$categoria_id)){
  				 	 if($verifica_produto_existe ){
				 	 	$retorno['resultado'] ='atualizado' ;
			   			$retorno['Mensagem'] = 'Registro da linha '.$linha_atual_arquivo.' inserido na fila  com sucesso!' ;

  				 	 }else{
  				 	 	$retorno['resultado'] ='inserido' ;
						$retorno['Mensagem'] = 'Registro da linha '.$linha_atual_arquivo.' inserido na fila com sucesso!' ;
  				 	 }
			     
			      }else{
			      	$retorno['resultado'] ='erro' ;
		 			$retorno['Mensagem'] ='Erro na linha '.$linha_atual_arquivo.', erro ao gravar no banco de dados';
			      }
	  		}	
	  }
	  

		// Chama o processo da fila de importação do arquivo
		InseriProdutosTabela::dispatch();

	    //retorna o Json
	    print_r(json_encode($retorno));
	   
		
	}


	/*

	  * Grava o produto na tabela
	  *
	  * @return Response
	  */
	public function grava_produto_fila($dados_linha_arquivo,$categoria_id) {

		$grava_produto_na_fila = new FilaProduto;
		$grava_produto_na_fila->string_linha_arquivo = utf8_decode($dados_linha_arquivo);
		$grava_produto_na_fila->categoria_produtos_id = $categoria_id;
		$grava_produto_na_fila->log_importacao = 'Enviado para a fila';
		if($grava_produto_na_fila->save()){
			return true;
		}else{
			return false;
		}
	}

	/*

	  * Verifica se a categoria existe 
	  *
	  * @return Response
	  */
	public function verifica_categoria_existe($categoria) {

	    $get_categoria = DB::table('categoria_produtos')->where('nome',$categoria)->get();
		$array_categoria = json_decode(json_encode($get_categoria), true);

	 	if(isset($array_categoria[0]['categoria_produtos_id'])){
	 		return $array_categoria[0]['categoria_produtos_id'];
	 	}else{

	 		 $grava_categoria_nova = new CategoriaProdutos;
		     $grava_categoria_nova->nome = $categoria;
		     $grava_categoria_nova->save();
		     return  $grava_categoria_nova->categoria_produtos_id;
		 }
	}

	/*

	  * Verifica se o produto existe 
	  *
	  * @return Response
	  */

	public function verifica_produto_existe($nome_produto) {

	    $get_nome_produto = DB::table('produtos')->where('nome',$nome_produto)->get();
		$array_produto_existe = json_decode(json_encode($get_nome_produto), true);

	 	if(isset($array_produto_existe[0]['produto_id'])){
	 		return true;
	 	}else{
	 		return false;
	 	}
	}


	/**
	  * Exibir uma listagem dos registros
	  *
	  * @return Response
	  */
	public function index(Request $request, $id = null) {
	     
	  /**********faz os filtros condicionais**********************/
	  $dados = $request->all();

	  if(!isset($dados['status'])){
	  	$status = 0;
	  }else{
	  	$status = $dados['status'];
	  }

	  if(!isset($dados['filtro'])){
	  	$filtro = '';
	  }else{
	  	$filtro = $dados['filtro'];
	  }

	  if(!isset($dados['limite'])){
	  	$limite = '9999999999999';
	  }else{
	  	$limite = $dados['limite'];
	  }

	  if(!isset($dados['pagina'])){
	  	$pagina = '1';
	  }else{
	  	$pagina = $dados['pagina'];
	  }
	 
	  $pagina = $pagina-1;
	  $offset = $pagina * $limite;
	  /**********faz os filtros condicionais**********************/


	  /******* comeca a montar a query **************************/
	  $query = new Produtos;

	   //monta a query
       $lista_produtos['lista'] = $query->select('produtos.produto_id','produtos.nome', 'produtos.peso' ,'produtos.cor','produtos.descricao','produtos.status','categoria_produtos.nome as categoria')
       									->join('categoria_produtos', 'categoria_produtos.categoria_produtos_id', '=', 'produtos.categoria_id')
				    					->orderBy('produtos.nome', 'asc')
				    					/*->Where('tb_usuarios.status', '=', $status)*/
				    					->Where('produtos.nome','like', '%' .$filtro. '%')
				    					->Where('produtos.status','=', $status)
				    					->orWhere('produtos.descricao','like', '%' .$filtro. '%')
				    					->Where('produtos.status','=', $status)
				    					->offset($offset)
				    					->limit($limite)
				      				    ->get();


		//Faz a mesma query de cima mas somente o count	      				    
        $lista_produtos['qtd_registros'] =   count( $query->select('produtos.produto_id','produtos.nome', 'produtos.peso' ,'produtos.cor','produtos.descricao','produtos.status')
       							     
				    					/*->Where('tb_usuarios.status', '=', $status)*/
				    					->Where('produtos.nome','like', '%' .$filtro. '%')
				    					->Where('produtos.status','=', $status)
				    					->orWhere('produtos.descricao','like', '%' .$filtro. '%')
				    					->Where('produtos.status','=', $status)
				      				    ->get());

	    print_r(json_encode($lista_produtos)); 		
	}
	 

	


	 
	 
	/**
	  * Editar um registro específico.
	  *
	  * @param  Request  $request
	  * @param  int  $id
	  * @return Response
	  */
	public function update(Request $request, $id) {

	     $post = new Produtos;
	 
	     $post->nome = $request->input('nome');
	     $post->peso = $request->input('peso');
	     $post->cor = $request->input('cor');
	     $post->descricao = $request->input('descricao');
	     $post->categoria_id = $request->input('categoria_id');
	     $post->exists = true;
	     $post->produto_id = $id;


	     if( $post->save()){
	     	$retorno[0] = true;
	     	$retorno['mensagem'] = 'Produto alterado com sucesso com o id #' .$request->input('id_registro');
	      }else{
	      	$retorno[0] = false;
	      	$retorno['mensagem'] = 'Falha ao alterado produto';
	      } 
	 
	     return $retorno;
	}
	 
	 
	/**
	  * Remover um registro específico.
	  *
	  * @param  int  $id
	  * @return Response
	  */
	public function destroy(Request $request, $id) {
	 
	     $medico = Produtos::find($id);
	     $medico->delete();
	 	 
	 	 $mensagem_retorno['mensagem'] = "Produto #" . $id. " excluído com sucesso.";
	     return json_encode($mensagem_retorno);
	}
	 


	 public function alterar_produtos(Request $request)
	{
	  

	  $retorno_view= array();
	  $dados = $request->all();	

	  /*Verifica se é uma alteração ou inclusao*/
	  if(isset($dados['id_registro'])){
	  	$dados_produto = Produtos::find($dados['id_registro']);
		$retorno_view['dados_produto']=$dados_produto;
	  }	       				   		   

	  	
	   
      /*Pega a lista de categorias*/
      $lista_categorias=  CategoriaProdutos::orderBy('nome', 'asc')
		      				   ->distinct('nome')->get();



	  $retorno_view['lista_categorias']=$lista_categorias; 


	  /*Verifica se o usuario esta autenticado, se nao estiver manda de volta pra pagina de login*/
	  return view('produtos.alterar_produtos',compact('retorno_view'));
      
	}

	//chama a view da pagina de pesquisa de produtos os dados vem via query da API
	public function pesquisa_produtos(){

	  /*Verifica se o usuario esta autenticado, se nao estiver manda de volta pra pagina de login*/
	  return view('produtos.pesquisa_produtos',compact('retorno_view'));

	}

	//chama a view da pagina de pesquisa de produtos os dados vem via query da API
	public function lista_processamento_produtos(){

	/*Verifica se o usuario esta autenticado, se nao estiver manda de volta pra pagina de login*/
	return view('produtos.pesquisa_fila_produtos',compact('retorno_view'));

  }
	
  
  /**
	  * Exibir uma listagem dos registros da fila de processamento
	  *
	  * @return Response
	  */
	public function gera_listas_fila_produtos(Request $request, $id = null) {
	     
		/**********faz os filtros condicionais**********************/
		$dados = $request->all();
  
		if(!isset($dados['status'])){
			$status = 0;
		}else{
			$status = $dados['status'];
		}
  
		if(!isset($dados['filtro'])){
			$filtro = '';
		}else{
			$filtro = $dados['filtro'];
		}
  
		if(!isset($dados['limite'])){
			$limite = '9999999999999';
		}else{
			$limite = $dados['limite'];
		}
  
		if(!isset($dados['pagina'])){
			$pagina = '1';
		}else{
			$pagina = $dados['pagina'];
		}
	   
		$pagina = $pagina-1;
		$offset = $pagina * $limite;
		/**********faz os filtros condicionais**********************/
  
  
		/******* comeca a montar a query **************************/
		$query = new FilaProduto;
  
		 //monta a query
		 $lista_produtos['lista'] = $query->select('fila_produtos.string_linha_arquivo','fila_produtos.log_importacao', 'fila_produtos.updated_at' ,'fila_produtos.processado')
										  ->orderBy('fila_produtos.updated_at', 'asc')
										  ->Where('fila_produtos.string_linha_arquivo','like', '%' .$filtro. '%')
										  ->orWhere('fila_produtos.log_importacao','like', '%' .$filtro. '%')
										  ->offset($offset)
										  ->limit($limite)
											->get();
  
  
		  //Faz a mesma query de cima mas somente o count	      				    
		  $lista_produtos['qtd_registros'] =   count( $query->select('fila_produtos.string_linha_arquivo')
										  
										  ->Where('fila_produtos.string_linha_arquivo','like', '%' .$filtro. '%')
										  ->orWhere('fila_produtos.log_importacao','like', '%' .$filtro. '%')
											->get());
  
		  print_r(json_encode($lista_produtos)); 		
	  }
	 
}
 