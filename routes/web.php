<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* Chama a home do sistema caso entre na raiz da pasta */
Route::get('/', 'HomeController@index');

Auth::routes();

/* Chama a home do sistema*/
Route::get('/home', 'HomeController@index')->name('home');

/* Chama a alteração dos Produtos*/
Route::get('/alterar_produtos', 'ProdutosController@alterar_produtos')->name('Alterar Produtos');

/* Chama a pesquisa de  produtos*/
Route::get('/pesquisar_produtos', 'ProdutosController@pesquisa_produtos')->name('Pesquisa de Produtos');

/* Chama  a pagina que envia  a Planilha de produtos*/
Route::get('/upload_planilha', 'ProdutosController@upload_planilha')->name('Upload da planilha de Produtos');

/* Chama  o upload da Planilha*/
Route::post('/recebe_arquivo_produtos', 'ProdutosController@recebe_arquivo_produtos')->name('Upload da planilha de Produtos');

/* Chama  a pagina que envia  a Planilha de produtos*/
Route::get('/fila_produtos', 'ProdutosController@lista_processamento_produtos')->name('Lista processamento de Produtos');

/* Chama  a pagina que envia  a Planilha de produtos*/
Route::get('/gera_lista_fila_produtos', 'ProdutosController@gera_listas_fila_produtos')->name('Gera via ajax a lista de processamento de Produtos');


//abaixo são as chamadas da API
Route::get('/api/v1/produtos/{id?}', 'ProdutosController@index');
Route::post('/api/v1/produtos', 'ProdutosController@store');
Route::post('/api/v1/produtos/{id}', 'ProdutosController@update');
Route::delete('/api/v1/produtos/{id}', 'ProdutosController@destroy');
